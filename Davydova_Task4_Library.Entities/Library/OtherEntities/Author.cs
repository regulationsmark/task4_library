﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Library.OtherEntities
{
    public class Author
    {
        public int Id{get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public Author(int id, string name, string surname)
        {
            Id = id;
            Name = name;
            Surname = surname;
        }
        //?
        public override bool Equals(object obj)
        {
            if (obj is Author author)
            {
                if (Id != 0)
                {
                    return this.Id == author.Id && this.Name == author.Name && this.Surname == author.Surname;
                }
            }
            return false;
        }

        //?
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        //?
        public override string ToString()
        {
            return $"{Name} {Surname}";
        }
    }
}
