﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Library.Publications
{
    public class NewsPaper:Publication
    {
        public int IssueNumber { get; set; }
        public DateTime IssueDate { get; set; }
        public  string ISSN { get; set; }

        public NewsPaper(int id, string name, int publicationYear, int countOfPages, string note, string placeOfPublication, string publishingHouse, int issueNumber, DateTime issueDate, string iSSN)
            :base(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse)
        {
            IssueNumber = issueNumber;
            IssueDate = issueDate;
            ISSN = iSSN;
        }

        public override bool Equals(object obj)
        {
            if (obj is NewsPaper newspaper)
            {
                if (!string.IsNullOrWhiteSpace(this.ISSN) && !string.IsNullOrWhiteSpace(newspaper.ISSN))
                {
                    return this.ISSN == newspaper.ISSN;
                }
                return this.Name == newspaper.Name && this.PublishingHouse == newspaper.PublishingHouse && this.IssueDate == newspaper.IssueDate;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string ToString(int i)
        {
            return $"Name: {Name} Number: {IssueNumber}" + Environment.NewLine +
                $"Place of publication: {PlaceOfPublication} Publishing House:{PublishingHouse}" + Environment.NewLine +
                $"Date of publication{IssueDate}";
        }
    }
}
