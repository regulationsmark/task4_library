﻿using Davydova_Task4_Library.BLL;
using Davydova_Task4_Library.DAL;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Davydova_Task4_Library.RealDAL;

namespace Davydova_Task4_Library.PL
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args);

            host.ConfigureServices((services) =>
            {
                services.AddAuthorDao()
                .AddAuthorRealDao()
                .AddBookRealDao()
                .AddBaseRealDao()
                .AddLibraryObjectRealDao()
                .AddPublicationRealDao()
                .AddLibraryObjectDao()
                .AddPublicationDao()
                .AddBaseDao()
                .AddBookDao()
                .AddNewspaperDao()
                .AddPatentDao()
                .AddAuthorLogic()
                .AddLibraryObjectLogic()
                .AddPublicationLogic()
                .AddBookLogic()
                .AddNewspaperLogic()
                .AddPatentLogic();
                services.AddHostedService<Worker>();
            });

            host.ConfigureLogging((hostingContext, loggingBuilder) =>
            {
                loggingBuilder.ClearProviders();

                var logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.File("data.log")
                    .WriteTo.Console()
                    .CreateLogger();

                loggingBuilder.AddSerilog(logger, dispose: true);
            });

            return host;
        }
    }
}
