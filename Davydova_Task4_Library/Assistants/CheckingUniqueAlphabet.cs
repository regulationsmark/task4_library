﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Alphabet;

namespace Davydova_Task4_Library.Assistants
{
    class CheckingUniqueAlphabet
    {
        private bool CheckingUniquenessOfAlphabet(string checkingString)
        {
            checkingString = checkingString.ToUpper();
            RussianAlphabet russianAlphabet = new RussianAlphabet();
            EnglishAlphabet englishAlphabet = new EnglishAlphabet();
            bool isRussian = false;
            bool isEnglish = false;
            foreach (char item in checkingString)
            {
                if (Char.IsLetter(item) && (englishAlphabet.alphabet.Contains(item)))
                {
                    isEnglish = true;
                    
                }
                if (Char.IsLetter(item) && (russianAlphabet.alphabet.Contains(item)))
                {
                    isRussian = true;
                }

            }

            return isRussian ^ isEnglish;
        }
    }
}
