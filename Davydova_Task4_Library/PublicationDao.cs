﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class PublicationDao : IPublicationDao
    {
        private List<Publication> _publications;
        private ILogger<PublicationDao> _logger;

        public PublicationDao(ILogger<PublicationDao> logger)
        {
            _logger = logger;
            _publications = new List<Publication>();
        }
        public int AddPublication(Publication publication)
        {
            _publications.Add(publication);
            _logger.LogDebug($"{_publications.Count} authors was created.");
            return _publications.Last().Id;
        }

        public bool DeletePublication(int publicationId)
        {
            foreach (Publication publication in _publications)
            {
                if (publication.Id == publicationId)
                {
                    _logger.LogDebug($"Publication with id {publicationId} was deleted.");
                    return _publications.Remove(publication);
                }
            }
            _logger.LogDebug($"Publication with id {publicationId} don`t exists.");
            return false;
        }

        public IEnumerable<Publication> GetAllPublications()
        {
            _logger.LogDebug($"{_publications.Count} publications returned. Ids: { JsonConvert.SerializeObject(_publications.Select(p => p.Id)) }");
            return _publications;
        }

        public Publication GetPublicationById(int publicationId)
        {
            foreach (Publication publication in _publications)
            {
                if (publication.Id == publicationId)
                {
                    _logger.LogDebug($"Publication with id {publicationId} was recieved.");
                    return publication;
                }
            }
            _logger.LogDebug($"Publication with id {publicationId} don`t exists.");
            return null;
        }

        public void UpdatePublication(Publication publication)
        {
            foreach (Publication publication1 in _publications)
            {
                if (publication1.Id == publication.Id)
                {
                    publication1.CountOfPages = publication.CountOfPages;
                    publication1.Name = publication.Name;
                    publication1.Note = publication.Note;
                    publication1.PlaceOfPublication = publication.PlaceOfPublication;
                    publication1.PublicationYear = publication.PublicationYear;
                    publication1.PublishingHouse = publication.PublishingHouse;
                }
            }
        }
    }
}
