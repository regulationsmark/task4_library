﻿using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.DAL_Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.DAL.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Davydova_Task4_Library.DAL
{
    public class AuthorDao:IAuthorDao
    {
        private List<Author> _authors;
        private ILogger<AuthorDao> _logger;

        public AuthorDao(ILogger<AuthorDao> logger)
        {
            _logger = logger;
            _authors = new List<Author>();
        }
        public Author GetAuthorById(int idAuthor)
        {
            foreach (Author author in _authors)
            {
                if (author.Id == idAuthor)
                {
                    _logger.LogDebug($"Author with id {idAuthor} was recieved.");
                    return author;
                }
            }
            _logger.LogDebug($"Author with id {idAuthor} don`t exists.");
            return null;
        }

        public int AddAuthor(Author author)
        {
            _authors.Add(author);
            _logger.LogDebug($"{_authors.Count} authors was created.");
            return _authors.Last().Id;
        }

        public bool DeleteAuthor(int authorId)
        {
            foreach (Author author in _authors)
            {
                if (author.Id == authorId)
                {
                    _logger.LogDebug($"Author with id {authorId} was deleted.");
                    return _authors.Remove(author);
                }
            }
            _logger.LogDebug($"Author with id {authorId} don`t exists.");
            return false;
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            _logger.LogDebug($"{_authors.Count} authors returned. Ids: { JsonConvert.SerializeObject(_authors.Select(p => p.Id)) }");
            return _authors;
        }

        public Author GetAuthorByName(string nameAuthor)
        {
            foreach (Author author in _authors)
            {
                if (author.Name == nameAuthor)
                {
                    _logger.LogDebug($"Author with name {nameAuthor} was recieved.");
                    return author;
                }
            }
            _logger.LogDebug($"Author with name {nameAuthor} don`t exists.");
            return null;
        }

        public void UpdateAuthor(Author author)
        {
            foreach (Author authorItem in _authors)
            {
                if (authorItem.Id == author.Id)
                {
                    authorItem.Name = author.Name;
                    authorItem.Surname = author.Surname;
                }
            }
        }

        public IEnumerable<Author> GetAuthorsByBookId(int bookId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Author> GetInventorsByPatentId(int patentId)
        {
            throw new NotImplementedException();
        }
    }
}
