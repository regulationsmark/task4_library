﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class NewspaperDao : INewspaperDao
    {
        private List<NewsPaper> _newsPapers;
        private ILogger<NewspaperDao> _logger;

        public NewspaperDao(ILogger<NewspaperDao> logger)
        {
            _logger = logger;
            _newsPapers = new List<NewsPaper>();
        }
        public int AddNewsPaper(NewsPaper newspaper)
        {
            _newsPapers.Add(newspaper);
            _logger.LogDebug($"{_newsPapers.Count} newspapers was created.");
            return _newsPapers.Last().Id;
        }

        public bool DeleteNewsPaper(int newspaperId)
        {
            foreach (NewsPaper newsPaper in _newsPapers)
            {
                if (newsPaper.Id == newspaperId)
                {
                    _logger.LogDebug($"Newspaper with id {newspaperId} was deleted.");
                    return _newsPapers.Remove(newsPaper);
                }
            }
            _logger.LogDebug($"Newspaper with id {newspaperId} don`t exists.");
            return false;
        }

        public IEnumerable<NewsPaper> GetAllNewspapers()
        {
            _logger.LogDebug($"{_newsPapers.Count} authors returned. Ids: {JsonConvert.SerializeObject(_newsPapers.Select(p => p.Id)) }");
            return _newsPapers;
        }

        public NewsPaper GetNewpaperById(int newspaperId)
        {
            foreach (NewsPaper newsPaper in _newsPapers)
            {
                if (newsPaper.Id == newspaperId)
                {
                    _logger.LogDebug($"Newspaper with id {newspaperId} was recieved.");
                    return newsPaper;
                }
            }
            _logger.LogDebug($"Newspaper with id {newspaperId} don`t exists.");
            return null;
        }
    }
}
