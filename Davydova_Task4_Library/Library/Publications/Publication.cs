﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Library.Publications
{
    public class Publication:LibraryObject
    {
        private string publishingHouse;
        private int publishingYear;

        public Publication(string name, int countOfPages, string note, string placeOfPublication, string publishingHouse, int publishingYear)
            :base(name, countOfPages, note)
        {
            PlaceOfPublication = placeOfPublication;
            PublishingHouse = publishingHouse;
            PublishingYear = publishingYear;
        }

        public override string PlaceOfPublication
        {
            get
            {
                return PlaceOfPublication;
            }
            set
            {
                PlaceOfPublication = value; //add all checking
            }
        }

        public string PublishingHouse
        {
            get
            {
                return publishingHouse;
            }
            set
            {
                if (value.Length > 300)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} must be less than 300");
                }
                publishingHouse = value;
            }
        }

        public int PublishingYear
        {
            get
            {
                return publishingYear;
            }
            set
            {
                DateTime curDate = DateTime.Now;
                if (value > curDate.Year)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} mustn't be more than current year");
                }
                publishingYear = value;
            }
        }
    }
}
