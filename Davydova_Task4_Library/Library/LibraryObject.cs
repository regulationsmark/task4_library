﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library
{
    abstract public class LibraryObject
    {
        private string name;
        abstract public string PlaceOfPublication { get; set; }
        private int countOfPages;
        private string note;

        public LibraryObject(string name, int countOfPages, string note)//? PlaceOfPublication
        {
            Name = name;
            CountOfPages = countOfPages;
            Note = note;
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value is null || value.Length > 300)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} must be not null and less than 300");
                }
                name = value;
            }
        }

        public int CountOfPages
        {
            get
            {
                return countOfPages;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} must be positive or null");
                }
                countOfPages = value;
            }
        }

        public string Note
        {
            get
            {
                return note;
            }
            set
            {
                if (value.Length > 2000)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} must be less than 2000");
                }
                name = value;
            }
        }
    }
}
