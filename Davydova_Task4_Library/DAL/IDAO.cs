﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.DAL
{
    interface IDAO
    {
        string AddEntry(LibraryObject libraryObject);
        string DeleteEntry(int id);

        IEnumerable<LibraryObject> GetAllEntries();
        IEnumerable<LibraryObject> GetAllEntriesByName(string name);
        IEnumerable<LibraryObject> GetSortedEntriesByPublicationYear(int year);
        IEnumerable<LibraryObject> GetReverseSortedEntriesByPublicationYear(int year);

        IEnumerable<LibraryObject> GetBooksByAuthor(Author author);
        IEnumerable<LibraryObject> GetPetentsByInventor(Author author);
        IEnumerable<LibraryObject> GetBooksAndPetentsByInventor(Author author);

        IEnumerable<LibraryObject> GetBooksByCharacterSet(string characterSet);
        IEnumerable<LibraryObject> GetBooksByCharacterSet(List<char> characterSet);

        IEnumerable<IEnumerable<LibraryObject>> GroupByPublicationYear(int year);

        bool IsUnique(LibraryObject libraryObject);

    }
}
