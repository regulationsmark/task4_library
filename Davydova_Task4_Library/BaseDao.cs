﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Davydova_Task4_Library.DAL
{
    public class BaseDao : IBaseDao
    {
        private List<LibraryObject> _libraryObjects;
        private ILogger<BaseDao> _logger;

        public BaseDao(ILogger<BaseDao> logger)
        {
            _logger = logger;
            _libraryObjects = new List<LibraryObject>();
        }

        public int AddLibraryObject(LibraryObject libraryObject)
        {
            _libraryObjects.Add(libraryObject);
            _logger.LogDebug($"{_libraryObjects.Count} library objects was created.");
            return _libraryObjects.Last().Id;
        }
        public bool DeleteLibraryObject(int idLibraryObject)
        {
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject.Id == idLibraryObject)
                {
                    _logger.LogDebug($"Librarн object with id {idLibraryObject} was deleted.");
                    return _libraryObjects.Remove(libraryObject);
                }
            }
            _logger.LogDebug($"Library object with id {idLibraryObject} don`t exists.");
            return false;
        }
        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            _logger.LogDebug($"{_libraryObjects.Count} library objects returned. Ids: { JsonConvert.SerializeObject(_libraryObjects.Select(p => p.Id)) }");
            return _libraryObjects;
        }
        public IEnumerable<LibraryObject> GetLibraryObjectByName(string nameLibraryObject)
        {
            List<LibraryObject> libraryObjects = new List<LibraryObject>();
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject.Name == nameLibraryObject)
                {
                    _logger.LogDebug($"Library object with name {nameLibraryObject} was recieved.");
                    libraryObjects.Add(libraryObject);
                }
            }
            _logger.LogDebug($"Library object with name {nameLibraryObject} don`t exists.");
            return libraryObjects;
        }
        public IEnumerable<LibraryObject> SortByPublicationYear()
        {
            _libraryObjects.Sort();
            _logger.LogDebug($"{_libraryObjects.Count} libary objects was sorted.");
            return _libraryObjects;
        }
        public IEnumerable<LibraryObject> ReverseSortByPublicationYear()
        {
            _libraryObjects.Sort();
            _libraryObjects.Reverse();
            _logger.LogDebug($"{_libraryObjects.Count} libary objects was sorted in reverse order.");
            return _libraryObjects;
        }
        public IEnumerable<IEnumerable<LibraryObject>> GroupByPublicationYear()
        {
            _logger.LogDebug($"{_libraryObjects.Count} libary objects was grouped by publication year.");
            return _libraryObjects.GroupBy(libObj => libObj.PublicationYear);
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            List<Book> books = new List<Book>();
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject is Book)
                {
                    foreach(Author author in ((Book)libraryObject).Authors)
                    {
                        if (author.Id == authorId)
                        {
                            books.Add((Book)libraryObject);
                        }
                    }
                }
            }
            _logger.LogDebug($"{books.Count} books was recieved by author id { authorId}.");
            _logger.LogDebug($"Recieved books { JsonConvert.SerializeObject(books)}.");
            return books;
        }

        public IEnumerable<Patent> GetPatentsByInventorId(int inventorId)
        {
            List<Patent> patents = new List<Patent>();
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject is Patent)
                {
                    foreach (Author inventor in ((Patent)libraryObject).Inventors)
                    {
                        if (inventor.Id == inventorId)
                        {
                            patents.Add((Patent)libraryObject);
                        }
                    }
                }
            }
            _logger.LogDebug($"{patents.Count} patents was recieved by inventor id {inventorId}.");
            _logger.LogDebug($"Recieved petents { JsonConvert.SerializeObject(patents)}.");
            return patents;
        }

        public IEnumerable<LibraryObject> GetBooksAndPatentsByAuthorId(int authorId)
        {
            List<LibraryObject> libraryObjects = new List<LibraryObject>();
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject is Book)
                {
                    foreach (Author author in ((Book)libraryObject).Authors)
                    {
                        if (author.Id == authorId)
                        {
                            libraryObjects.Add(libraryObject);
                        }
                    }
                }
                if (libraryObject is Patent)
                {
                    foreach (Author inventor in ((Patent)libraryObject).Inventors)
                    {
                        if (inventor.Id == authorId)
                        {
                            libraryObjects.Add(libraryObject);
                        }
                    }
                }
            }
            _logger.LogDebug($"{libraryObjects.Count} books and patents was recieved by inentor id {authorId}.");
            _logger.LogDebug($"Recieved books and petents { JsonConvert.SerializeObject(libraryObjects)}.");
            return libraryObjects;
        }

        public IEnumerable<Book> GetBooksStartByChars(string publishingHouse)
        {
            List<Book> books = new List<Book>();
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject is Book)
                {
                    if (((Book)libraryObject).PublishingHouse.StartsWith(publishingHouse))
                    {
                        books.Add((Book)libraryObject);
                    }
                }
            }
            _logger.LogDebug($"{books.Count} books was group by publishing house {publishingHouse}. Ids: { JsonConvert.SerializeObject(books.Select(p => p.Id)) }");
            //return books.GroupBy(books => books.PublishingHouse);
            return books;
        }

        IEnumerable<LibraryObject> IBaseDao.GroupByPublicationYear()
        {
            throw new NotImplementedException();
        }
    }
}
