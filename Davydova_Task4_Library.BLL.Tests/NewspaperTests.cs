﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.DAL;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Tests
{
    [TestFixture]
    public class NewspaperTests
    {
        private INewspaperLogic _newspaperLogic;

        [SetUp]
        public void StartUp()
        {
            var fakeNewspaperDaoLogger = new Mock<ILogger<NewspaperDao>>();
            var fakeBaseDaoLogger = new Mock<ILogger<BaseDao>>();
            var fakeNewspaperValidationLogger = new Mock<ILogger<NewsPaperLogic>>();
            INewspaperDao newspaperDao = new NewspaperDao(fakeNewspaperDaoLogger.Object);
            IBaseDao baseDao = new BaseDao(fakeBaseDaoLogger.Object);
            _newspaperLogic = new NewsPaperLogic(newspaperDao, baseDao, fakeNewspaperValidationLogger.Object);
        }

        [Test]
        public void AddNewspaperTest()
        {
            var result = _newspaperLogic.GetAllNewsPapers();
            var count = result.Count();

            int id = 1;
            string name = "The New York Times";
            string publicationYear = "2021";
            string countOfPages = "20";
            string note = "The New York Times is an American daily newspaper based in New York City with a worldwide readership.";
            string placeOfPublication = "New York";
            string publishingHouse = "The Catastrophe of Success";
            int issueNumber = 2512;
            DateTime issueDate = new DateTime(2021, 02, 05);
            string iSSN = "ISSN 1564-7895";

            int newspaperId = _newspaperLogic.AddNewspaper(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN);

            result = _newspaperLogic.GetAllNewsPapers();
            Assert.AreEqual(count + 1, result.Count());
            Assert.AreEqual(id, newspaperId);
        }

        [Test]
        public void AddWhiteSpaceNewspaperTest()
        {
            var result = _newspaperLogic.GetAllNewsPapers();
            var count = result.Count();

            int id = 2;
            string name = "The Times";
            string publicationYear = " ";
            string countOfPages = " ";
            string note = " ";
            string placeOfPublication = " ";
            string publishingHouse = " ";
            int issueNumber = 2512;
            DateTime issueDate = new DateTime();
            string iSSN = "ISSN 1564-7895";

            _newspaperLogic.AddNewspaper(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN);

            result = _newspaperLogic.GetAllNewsPapers();
            Assert.AreEqual(count, result.Count());
        }

        [Test]
        public void DeleteNespaperTest()
        {
            int id = 1;
            string name = "The New York Times";
            string publicationYear = "2021";
            string countOfPages = "20";
            string note = "The New York Times is an American daily newspaper based in New York City with a worldwide readership.";
            string placeOfPublication = "New York";
            string publishingHouse = "The Catastrophe of Success";
            int issueNumber = 2512;
            DateTime issueDate = new DateTime(2021, 02, 05);
            string iSSN = "ISSN 1564-7895";

            int newspaperId = _newspaperLogic.AddNewspaper(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN);

            int newId = 1;
            var result = _newspaperLogic.GetAllNewsPapers();
            var count = result.Count();

            bool isDelete = _newspaperLogic.DeleteNewspaper(newId);

            result = _newspaperLogic.GetAllNewsPapers();
            Assert.AreEqual(count - 1, result.Count());
            Assert.IsTrue(isDelete);
        }

        [Test]
        public void DeleteNonExistentNewspaperTest()
        {
            int id = 2;
            var result = _newspaperLogic.GetAllNewsPapers();
            var count = result.Count();

            bool isDelete = _newspaperLogic.DeleteNewspaper(id);

            result = _newspaperLogic.GetAllNewsPapers();
            Assert.AreEqual(count, result.Count());
            Assert.IsFalse(isDelete);
        }

        [Test]
        public void GetAllNewspapersTest()
        {
            int id = 1;
            string name = "The New York Times";
            string publicationYear = "2021";
            string countOfPages = "20";
            string note = "The New York Times is an American daily newspaper based in New York City with a worldwide readership.";
            string placeOfPublication = "New York";
            string publishingHouse = "The Catastrophe of Success";
            int issueNumber = 2512;
            DateTime issueDate = new DateTime(2021, 02, 05);
            string iSSN = "ISSN 1564-7895";

            int newspaperId = _newspaperLogic.AddNewspaper(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN);

            var result = _newspaperLogic.GetAllNewsPapers();
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void GetNewspaperByIdTest()
        {
            int id = 1;
            string name = "The New York Times";
            string publicationYear = "2021";
            string countOfPages = "20";
            string note = "The New York Times is an American daily newspaper based in New York City with a worldwide readership.";
            string placeOfPublication = "New York";
            string publishingHouse = "The Catastrophe of Success";
            int issueNumber = 2512;
            DateTime issueDate = new DateTime(2021, 02, 05);
            string iSSN = "ISSN 1564-7895";

            int newspaperId = _newspaperLogic.AddNewspaper(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN);

            int newId = 1;
            var result = _newspaperLogic.GetNewspaperById(newId);
            Assert.AreEqual(id, result.Id);
        }

        [Test]
        public void GetNonExistedNewspaperByIdTest()
        {
            int id = 2;
            var result = _newspaperLogic.GetNewspaperById(id);
            Assert.AreEqual(null, result);
        }
    }
}
