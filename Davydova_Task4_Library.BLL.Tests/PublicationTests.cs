﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.DAL;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Tests
{
    [TestFixture]
    public class PublicationTests
    {
        private IPublicationLogic _publicationLogic;

        [SetUp]
        public void StartUp()
        {
            var fakePublicationDaoLogger = new Mock<ILogger<PublicationDao>>();
            var fakeBaseDaoLogger = new Mock<ILogger<BaseDao>>();
            var fakePublicationLogicLogger = new Mock<ILogger<PublicationLogic>>();
            IPublicationDao publicationDao = new PublicationDao(fakePublicationDaoLogger.Object);
            IBaseDao baseDao = new BaseDao(fakeBaseDaoLogger.Object);
            _publicationLogic = new PublicationLogic(publicationDao, baseDao, fakePublicationLogicLogger.Object);
        }

        [Test]
        public void AddPublicationTest()
        {
            var result = _publicationLogic.GetAllPublication();
            var count = result.Count();

            int id = 1;
            string name = "First publication";
            string publicationYear = "2021";
            string countOfPages = "58";
            string note = "First publication about library.";
            string placeOfPublication = "Saratov";
            string publishingHouse = "SRTV";
            
            int publicationId = _publicationLogic.AddPublication(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse);

            result = _publicationLogic.GetAllPublication();
            Assert.AreEqual(count + 1, result.Count());
            Assert.AreEqual(id, publicationId);
        }

        [Test]
        public void AddWhiteSpaceLibraryTest()
        {
            var result = _publicationLogic.GetAllPublication();
            var count = result.Count();

            int id = 2;
            string name = " ";
            string publicationYear = " ";
            string countOfPages = " ";
            string note = " ";
            string placeOfPublication = " ";
            string publishingHouse = " ";

            _publicationLogic.AddPublication(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse);

            result = _publicationLogic.GetAllPublication();
            Assert.AreEqual(count, result.Count());
        }

        [Test]
        public void DeletePublicationTest()
        {
            int id = 1;
            string name = "First publication";
            string publicationYear = "2021";
            string countOfPages = "58";
            string note = "First publication about library.";
            string placeOfPublication = "Saratov";
            string publishingHouse = "SRTV";

            int publicationId = _publicationLogic.AddPublication(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse);

            int newId = 1;
            var result = _publicationLogic.GetAllPublication();
            var count = result.Count();

            bool isDelete = _publicationLogic.DeletePublication(newId);

            result = _publicationLogic.GetAllPublication();
            Assert.AreEqual(count - 1, result.Count());
            Assert.IsTrue(isDelete);
        }

        [Test]
        public void DeleteNonExistentPublicationTest()
        {
            int id = 2;
            var result = _publicationLogic.GetAllPublication();
            var count = result.Count();

            bool isDelete = _publicationLogic.DeletePublication(id);

            result = _publicationLogic.GetAllPublication();
            Assert.AreEqual(count, result.Count());
            Assert.IsFalse(isDelete);
        }

        [Test]
        public void GetAllPublicationsTest()
        {
            int id = 1;
            string name = "First publication";
            string publicationYear = "2021";
            string countOfPages = "58";
            string note = "First publication about library.";
            string placeOfPublication = "Saratov";
            string publishingHouse = "SRTV";

            int publicationId = _publicationLogic.AddPublication(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse);

            var result = _publicationLogic.GetAllPublication();
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void GetPublicationByIdTest()
        {
            int id = 1;
            string name = "First publication";
            string publicationYear = "2021";
            string countOfPages = "58";
            string note = "First publication about library.";
            string placeOfPublication = "Saratov";
            string publishingHouse = "SRTV";

            int publicationId = _publicationLogic.AddPublication(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse);

            int newId = 1;
            var result = _publicationLogic.GetPublicationById(newId);
            Assert.AreEqual(id, result.Id);
        }

        [Test]
        public void GetNullLibraryObjectByIdTest()
        {
            int id = 2;
            var result = _publicationLogic.GetPublicationById(id);
            Assert.AreEqual(null, result);
        }
    }
}
