﻿using Davydova_Task4_Library.BLL.Implementations;
using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Tests
{
    [TestFixture]
    public class BookTests
    {
        private IBookLogic _bookLogic;
        private ILibraryObjectLogic _libraryObjectLogic;
        private IAuthorLogic _authorLogic;

        [SetUp]
        public void StartUp()
        {
            var fakeBookDaoLogger = new Mock<ILogger<BookDao>>();
            var fakeBaseDaoLogger = new Mock<ILogger<BaseDao>>();
            var fakeAuthorDaoLogger = new Mock<ILogger<AuthorDao>>();
            var fakeAuthorValidationLogger = new Mock<ILogger<AuthorLogic>>();
            var fakeBookValidationLogger = new Mock<ILogger<BookLogic>>();
            IBookDao bookDao = new BookDao(fakeBookDaoLogger.Object);
            IBaseDao baseDao = new BaseDao(fakeBaseDaoLogger.Object);
            IAuthorDao authorDao = new AuthorDao(fakeAuthorDaoLogger.Object);
            _bookLogic = new BookLogic(bookDao, baseDao, authorDao, fakeBookValidationLogger.Object);
            _authorLogic = new AuthorLogic(authorDao, fakeAuthorValidationLogger.Object);
        }

        [Test]
        public void AddBookTest()
        {
            var result = _bookLogic.GetAllBooks();
            var count = result.Count();

            List<Author> authors = new List<Author>();

            int idAuthor = 1;
            string nameAuthor = "Fedor";
            string surnameAuthor = "Dostoevsky";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            authors.Add(author);
            _authorLogic.AddAuthor(idAuthor, nameAuthor, surnameAuthor);
                    
            int id = 1;
            string name = "Crime and Punishment";
            string countOfPages = "357";
            string note = "A novel in six parts with an epilogue";
            string placeOfPublication = "Moscow";
            string publishingHouse = "Azbuka";
            string publishingYear = "2021";
            string iSBN = "ISBN 950-120-110-0";

            int bookId = _bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, authors, iSBN);

            result = _bookLogic.GetAllBooks();
            Assert.AreEqual(count + 1, result.Count());
            Assert.AreEqual(id, bookId);
        }

        [Test]
        public void AddWhiteSpaceBookTest()
        {
            var result = _bookLogic.GetAllBooks();
            var count = result.Count();

            List<Author> authors = new List<Author>();

            int idAuthor = 1;
            string nameAuthor = "Fedor";
            string surnameAuthor = "Dostoevsky";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            authors.Add(author);
            _authorLogic.AddAuthor(idAuthor, nameAuthor, surnameAuthor);

            int id = 2;
            string name = " ";
            string countOfPages = " ";
            string note = "A novel in six parts with an epilogue";
            string placeOfPublication = " ";
            string publishingHouse = " ";
            string publishingYear = " ";
            string iSBN = " ";

            _bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, authors, iSBN);

            result = _bookLogic.GetAllBooks();
            Assert.AreEqual(count, result.Count());
        }

        [Test]
        public void DeleteBookTest()
        {
            List<Author> authors = new List<Author>();

            int idAuthor = 1;
            string nameAuthor = "Fedor";
            string surnameAuthor = "Dostoevsky";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            authors.Add(author);
            _authorLogic.AddAuthor(idAuthor, nameAuthor, surnameAuthor);

            int id = 1;
            string name = "Crime and Punishment";
            string countOfPages = "357";
            string note = "A novel in six parts with an epilogue";
            string placeOfPublication = "Moscow";
            string publishingHouse = "Azbuka";
            string publishingYear = "2021";
            string iSBN = "ISBN 950-120-110-0";

            int bookId = _bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, authors, iSBN);


            int deletedId = 1;
            var result = _bookLogic.GetAllBooks();
            var count = result.Count();

            bool isDelete = _bookLogic.DeleteBook(deletedId);

            result = _bookLogic.GetAllBooks();
            Assert.AreEqual(count - 1, result.Count());
            Assert.IsTrue(isDelete);
        }

        [Test]
        public void DeleteNonExistentBookTest()
        {
            int id = 2;
            var result = _bookLogic.GetAllBooks();
            var count = result.Count();

            bool isDelete = _bookLogic.DeleteBook(id);

            result = _bookLogic.GetAllBooks();
            Assert.AreEqual(count, result.Count());
            Assert.IsFalse(isDelete);
        }

        [Test]
        public void GetAllBooksTest()
        {
            List<Author> authors1 = new List<Author>();
            List<Author> authors2 = new List<Author>();
            List<Author> authors3 = new List<Author>();

            int idAuthor1 = 1;
            string nameAuthor1 = "Fedor";
            string surnameAuthor1 = "Dostoevsky";

            int idAuthor2 = 2;
            string nameAuthor2 = "Alexandr";
            string surnameAuthor2 = "Pushkin";

            int idAuthor3 = 3;
            string nameAuthor3 = "Leo";
            string surnameAuthor3 = "Tolstoy";

            Author author1 = new(idAuthor1, nameAuthor1, surnameAuthor1);
            Author author2 = new(idAuthor2, nameAuthor2, surnameAuthor2);
            Author author3 = new(idAuthor3, nameAuthor3, surnameAuthor3);
            authors1.Add(author1);
            authors2.Add(author2);
            authors3.Add(author3);

            _authorLogic.AddAuthor(idAuthor1, nameAuthor1, surnameAuthor1);
            _authorLogic.AddAuthor(idAuthor2, nameAuthor2, surnameAuthor2);
            _authorLogic.AddAuthor(idAuthor3, nameAuthor3, surnameAuthor3);

            int id1 = 1;
            string name1 = "Crime and Punishment";
            string countOfPages1 = "357";
            string note1 = "A novel in six parts with an epilogue.";
            string placeOfPublication1 = "Moscow";
            string publishingHouse1 = "Azbuka";
            string publishingYear1 = "2021";
            string iSBN1 = "ISBN 950-120-110-0";

            int id2 = 2;
            string name2 = "Captain's daughter";
            string countOfPages2 = "160";
            string note2 = "The Captain's Daughter is a historical novel by the Russian writer Alexander Pushkin.";
            string placeOfPublication2 = "Moscow";
            string publishingHouse2 = "Nigma";
            string publishingYear2 = "2014";
            string iSBN2 = "ISBN 9550-15-110-0";

            int id3 = 3;
            string name3 = "War and Peace";
            string countOfPages3 = "1225";
            string note3 = "War and Peace is a novel by the Russian author Leo Tolstoy, first published serially, then published in its entirety in 1869.";
            string placeOfPublication3 = "Moscow";
            string publishingHouse3 = "Azbuka";
            string publishingYear3 = "2020";
            string iSBN3 = "ISBN 9850-130-17-0";

            int bookId1 = _bookLogic.AddBook(id1, name1, countOfPages1, note1, placeOfPublication1, publishingHouse1, publishingYear1, authors1, iSBN1);
            int bookId2 = _bookLogic.AddBook(id2, name2, countOfPages2, note2, placeOfPublication2, publishingHouse2, publishingYear2, authors2, iSBN2);
            int bookId3 = _bookLogic.AddBook(id3, name3, countOfPages3, note3, placeOfPublication3, publishingHouse3, publishingYear3, authors3, iSBN3);


            var result = _bookLogic.GetAllBooks();
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void GetBooksByIdAuthorTest()
        {
            List<Author> authors = new List<Author>();
            
            int idAuthor = 1;
            string nameAuthor = "Fedor";
            string surnameAuthor = "Dostoevsky";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            authors.Add(author);
            _authorLogic.AddAuthor(idAuthor, nameAuthor, surnameAuthor);

            int id = 1;
            string name = "Crime and Punishment";
            string countOfPages = "357";
            string note = "A novel in six parts with an epilogue.";
            string placeOfPublication = "Moscow";
            string publishingHouse = "Azbuka";
            string publishingYear = "2021";
            string iSBN = "ISBN 950-120-110-0";

            _bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, authors, iSBN);

            int newId = 1;
            var result = _bookLogic.GetBooksByIdAuthor(newId);
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void GetBooksByIdNonExistentAuthorTest()
        {
            int id = 2;
            var result = _bookLogic.GetBooksByIdAuthor(id);
            Assert.IsEmpty(result);
        }
        
        [Test]
        public void GetBooksByNameTest()
        {
            List<Author> authors = new List<Author>();

            int idAuthor = 1;
            string nameAuthor = "Fedor";
            string surnameAuthor = "Dostoevsky";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            authors.Add(author);
            _authorLogic.AddAuthor(idAuthor, nameAuthor, surnameAuthor);

            int id = 1;
            string name = "Crime and Punishment";
            string countOfPages = "357";
            string note = "A novel in six parts with an epilogue.";
            string placeOfPublication = "Moscow";
            string publishingHouse = "Azbuka";
            string publishingYear = "2021";
            string iSBN = "ISBN 950-120-110-0";

            _bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, authors, iSBN);

            string newNameBook = "Crime and Punishment";
            var result = _bookLogic.GetBooksByName(newNameBook);
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void GetBooksByNonExistentNameTest()
        {
            string name = "Pride and Prejudice";
            var result = _bookLogic.GetBooksByName(name);
            Assert.IsEmpty(result);
        }
        
        [Test]
        public void GetBooksByNameAuthorTest()
        {
            List<Author> authors = new List<Author>();

            int idAuthor = 1;
            string nameAuthor = "Fedor";
            string surnameAuthor = "Dostoevsky";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            authors.Add(author);
            _authorLogic.AddAuthor(idAuthor, nameAuthor, surnameAuthor);

            int id = 1;
            string name = "Crime and Punishment";
            string countOfPages = "357";
            string note = "A novel in six parts with an epilogue.";
            string placeOfPublication = "Moscow";
            string publishingHouse = "Azbuka";
            string publishingYear = "2021";
            string iSBN = "ISBN 950-120-110-0";

            _bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, authors, iSBN);

            string newNameAuthor = "Fedor";
            var result = _bookLogic.GetBooksByNameAuthor(newNameAuthor);
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void GetBooksByNonExistentAuthorNameTest()
        {
            string nameAuthor = "Alex";
            var result = _bookLogic.GetBooksByNameAuthor(nameAuthor);
            Assert.IsEmpty(result);
        }

        [Test]
        public void GetSortedBookByPublicationYearTest()
        {
            List<Author> authors1 = new List<Author>();
            List<Author> authors2 = new List<Author>();
            List<Author> authors3 = new List<Author>();

            int idAuthor1 = 1;
            string nameAuthor1 = "Fedor";
            string surnameAuthor1 = "Dostoevsky";

            int idAuthor2 = 2;
            string nameAuthor2 = "Alexandr";
            string surnameAuthor2 = "Pushkin";

            int idAuthor3 = 3;
            string nameAuthor3 = "Leo";
            string surnameAuthor3 = "Tolstoy";

            Author author1 = new(idAuthor1, nameAuthor1, surnameAuthor1);
            Author author2 = new(idAuthor2, nameAuthor2, surnameAuthor2);
            Author author3 = new(idAuthor3, nameAuthor3, surnameAuthor3);
            authors1.Add(author1);
            authors2.Add(author2);
            authors3.Add(author3);

            _authorLogic.AddAuthor(idAuthor1, nameAuthor1, surnameAuthor1);
            _authorLogic.AddAuthor(idAuthor2, nameAuthor2, surnameAuthor2);
            _authorLogic.AddAuthor(idAuthor3, nameAuthor3, surnameAuthor3);

            int id1 = 1;
            string name1 = "Crime and Punishment";
            string countOfPages1 = "357";
            string note1 = "A novel in six parts with an epilogue.";
            string placeOfPublication1 = "Moscow";
            string publishingHouse1 = "Azbuka";
            string publishingYear1 = "2021";
            string iSBN1 = "ISBN 950-120-110-0";

            int id2 = 2;
            string name2 = "Captain's daughter";
            string countOfPages2 = "160";
            string note2 = "The Captain's Daughter is a historical novel by the Russian writer Alexander Pushkin.";
            string placeOfPublication2 = "Moscow";
            string publishingHouse2 = "Nigma";
            string publishingYear2 = "2014";
            string iSBN2 = "ISBN 9550-15-110-0";

            int id3 = 3;
            string name3 = "War and Peace";
            string countOfPages3 = "1225";
            string note3 = "War and Peace is a novel by the Russian author Leo Tolstoy, first published serially, then published in its entirety in 1869.";
            string placeOfPublication3 = "Moscow";
            string publishingHouse3 = "Azbuka";
            string publishingYear3 = "2020";
            string iSBN3 = "ISBN 9850-130-17-0";

            _bookLogic.AddBook(id1, name1, countOfPages1, note1, placeOfPublication1, publishingHouse1, publishingYear1, authors1, iSBN1);
            _bookLogic.AddBook(id2, name2, countOfPages2, note2, placeOfPublication2, publishingHouse2, publishingYear2, authors2, iSBN2);
            _bookLogic.AddBook(id3, name3, countOfPages3, note3, placeOfPublication3, publishingHouse3, publishingYear3, authors3, iSBN3);

            var initialState = _bookLogic.GetAllBooks().ToList();
            initialState.Sort();
            _bookLogic.GetSortedBookByPublicationYear();
            var finalState = _bookLogic.GetAllBooks();

            bool equal = initialState.SequenceEqual(finalState);

            Assert.IsTrue(equal);
        }

        [Test]
        public void GetReverseSortedBookByPublicationYearTest()
        {
            List<Author> authors1 = new List<Author>();
            List<Author> authors2 = new List<Author>();
            List<Author> authors3 = new List<Author>();

            int idAuthor1 = 1;
            string nameAuthor1 = "Fedor";
            string surnameAuthor1 = "Dostoevsky";

            int idAuthor2 = 2;
            string nameAuthor2 = "Alexandr";
            string surnameAuthor2 = "Pushkin";

            int idAuthor3 = 3;
            string nameAuthor3 = "Leo";
            string surnameAuthor3 = "Tolstoy";

            Author author1 = new(idAuthor1, nameAuthor1, surnameAuthor1);
            Author author2 = new(idAuthor2, nameAuthor2, surnameAuthor2);
            Author author3 = new(idAuthor3, nameAuthor3, surnameAuthor3);
            authors1.Add(author1);
            authors2.Add(author2);
            authors3.Add(author3);

            _authorLogic.AddAuthor(idAuthor1, nameAuthor1, surnameAuthor1);
            _authorLogic.AddAuthor(idAuthor2, nameAuthor2, surnameAuthor2);
            _authorLogic.AddAuthor(idAuthor3, nameAuthor3, surnameAuthor3);

            int id1 = 1;
            string name1 = "Crime and Punishment";
            string countOfPages1 = "357";
            string note1 = "A novel in six parts with an epilogue.";
            string placeOfPublication1 = "Moscow";
            string publishingHouse1 = "Azbuka";
            string publishingYear1 = "2021";
            string iSBN1 = "ISBN 950-120-110-0";

            int id2 = 2;
            string name2 = "Captain's daughter";
            string countOfPages2 = "160";
            string note2 = "The Captain's Daughter is a historical novel by the Russian writer Alexander Pushkin.";
            string placeOfPublication2 = "Moscow";
            string publishingHouse2 = "Nigma";
            string publishingYear2 = "2014";
            string iSBN2 = "ISBN 9550-15-110-0";

            int id3 = 3;
            string name3 = "War and Peace";
            string countOfPages3 = "1225";
            string note3 = "War and Peace is a novel by the Russian author Leo Tolstoy, first published serially, then published in its entirety in 1869.";
            string placeOfPublication3 = "Moscow";
            string publishingHouse3 = "Azbuka";
            string publishingYear3 = "2020";
            string iSBN3 = "ISBN 9850-130-17-0";

            _bookLogic.AddBook(id1, name1, countOfPages1, note1, placeOfPublication1, publishingHouse1, publishingYear1, authors1, iSBN1);
            _bookLogic.AddBook(id2, name2, countOfPages2, note2, placeOfPublication2, publishingHouse2, publishingYear2, authors2, iSBN2);
            _bookLogic.AddBook(id3, name3, countOfPages3, note3, placeOfPublication3, publishingHouse3, publishingYear3, authors3, iSBN3);

            var initialState = _bookLogic.GetAllBooks().ToList();
            initialState.Sort();
            initialState.Reverse();
            _bookLogic.GetReverseSortedBookByPublicationYear();
            var finalState = _bookLogic.GetAllBooks();

            bool equal = initialState.SequenceEqual(finalState);

            Assert.IsTrue(equal);
        }

        [Test]
        public void GroupByPublicationYearTest()
        {
            List<Author> authors1 = new List<Author>();
            List<Author> authors2 = new List<Author>();
            List<Author> authors3 = new List<Author>();

            int idAuthor1 = 1;
            string nameAuthor1 = "Fedor";
            string surnameAuthor1 = "Dostoevsky";

            int idAuthor2 = 2;
            string nameAuthor2 = "Alexandr";
            string surnameAuthor2 = "Pushkin";

            int idAuthor3 = 3;
            string nameAuthor3 = "Leo";
            string surnameAuthor3 = "Tolstoy";

            Author author1 = new(idAuthor1, nameAuthor1, surnameAuthor1);
            Author author2 = new(idAuthor2, nameAuthor2, surnameAuthor2);
            Author author3 = new(idAuthor3, nameAuthor3, surnameAuthor3);
            authors1.Add(author1);
            authors2.Add(author2);
            authors3.Add(author3);

            _authorLogic.AddAuthor(idAuthor1, nameAuthor1, surnameAuthor1);
            _authorLogic.AddAuthor(idAuthor2, nameAuthor2, surnameAuthor2);
            _authorLogic.AddAuthor(idAuthor3, nameAuthor3, surnameAuthor3);

            int id1 = 1;
            string name1 = "Crime and Punishment";
            string countOfPages1 = "357";
            string note1 = "A novel in six parts with an epilogue.";
            string placeOfPublication1 = "Moscow";
            string publishingHouse1 = "Azbuka";
            string publishingYear1 = "2021";
            string iSBN1 = "ISBN 950-120-110-0";

            int id2 = 2;
            string name2 = "Captain's daughter";
            string countOfPages2 = "160";
            string note2 = "The Captain's Daughter is a historical novel by the Russian writer Alexander Pushkin.";
            string placeOfPublication2 = "Moscow";
            string publishingHouse2 = "Nigma";
            string publishingYear2 = "2014";
            string iSBN2 = "ISBN 9550-15-110-0";

            int id3 = 3;
            string name3 = "War and Peace";
            string countOfPages3 = "1225";
            string note3 = "War and Peace is a novel by the Russian author Leo Tolstoy, first published serially, then published in its entirety in 1869.";
            string placeOfPublication3 = "Moscow";
            string publishingHouse3 = "Azbuka";
            string publishingYear3 = "2020";
            string iSBN3 = "ISBN 9850-130-17-0";

            _bookLogic.AddBook(id1, name1, countOfPages1, note1, placeOfPublication1, publishingHouse1, publishingYear1, authors1, iSBN1);
            _bookLogic.AddBook(id2, name2, countOfPages2, note2, placeOfPublication2, publishingHouse2, publishingYear2, authors2, iSBN2);
            _bookLogic.AddBook(id3, name3, countOfPages3, note3, placeOfPublication3, publishingHouse3, publishingYear3, authors3, iSBN3);

            var initialState = _bookLogic.GetAllBooks().ToList();
            initialState.GroupBy(libObj => libObj.PublicationYear);
            _bookLogic.GroupByPublicationYear();
            var finalState = _bookLogic.GetAllBooks();

            bool equal = initialState.SequenceEqual(finalState);

            Assert.IsTrue(equal);
        }

        [Test]
        public void GetBooksByCharacterSetTest()
        {
            List<Author> authors = new List<Author>();
            int idAuthor = 1;
            string nameAuthor = "Fedor";
            string surnameAuthor = "Dostoevsky";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            authors.Add(author);

            int id = 1;
            string name = "Crime and Punishment";
            string countOfPages = "357";
            string note = "A novel in six parts with an epilogue.";
            string placeOfPublication = "Moscow";
            string publishingHouse = "Azbuka";
            string publishingYear = "2021";
            string iSBN = "ISBN 950-120-110-0";

            _bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, authors, iSBN);

            string enterString = "Azb";
            List<Book> books = new List<Book>();
            var result = _bookLogic.GetAllBooks();
            foreach (var book in result)
            {
                if (book.PublishingHouse.StartsWith(enterString))
                {
                    books.Add(book);
                }
            }

            Assert.IsNotEmpty(books);
        }

        [Test]
        public void GetBooksByNullCharacterSetTest()
        {
            string enterString = " ";
            List<Book> books = new List<Book>();
            var result = _bookLogic.GetAllBooks();
            foreach (var book in result)
            {
                if (book.PublishingHouse.StartsWith(enterString))
                {
                    books.Add(book);
                }
            }
            Assert.IsEmpty(books);
        }
    }
}
