﻿using Davydova_Task4_Library.Library.OtherEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Interfaces
{
    public interface IAuthorLogic
    {
        int AddAuthor(int id, string name, string surname);

        bool DeleteAuthor(int authorId);

        Author GetAuthorById(int idAuthor);

        IEnumerable<Author> GetAllAuthors();


    }
}
