﻿using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Interfaces
{
    public interface IPatientLogic
    {
        int AddPatent(int id, string name, string countOfPages, string note, string placeOfPublication, List<Author> inventors, string registrationNumber, DateTime applicationDate, DateTime publicationDate, string country);

        bool DeletePatent(int authorId);

        Patent GetPatentById(int idPatent);

        IEnumerable<Patent> GetAllPatents();

        IEnumerable<Patent> GetPatentsByNameInventor(string name);
    }
}
