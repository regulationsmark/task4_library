﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class LibraryObjectLogic:ILibraryObjectLogic
    {
        private ILibraryObject _libraryObjectDao;
        private IBaseDao _baseDao;
        private ILogger<LibraryObjectLogic> _logger;

        public LibraryObjectLogic(ILibraryObject libraryObjectDao, IBaseDao baseDao, ILogger<LibraryObjectLogic> logger)
        {
            _libraryObjectDao = libraryObjectDao;
            _baseDao = baseDao;
            _logger = logger;
        }

        public int AddLibraryObject(int id, string name, int publicationYear, string countOfPages, string note)
        {
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();
            
            if (libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note))
            {
                LibraryObject libraryObject = new LibraryObject(id, name, publicationYear, int.Parse(countOfPages), note);
                _baseDao.AddLibraryObject(libraryObject);
                return _libraryObjectDao.AddLibraryObject(libraryObject);
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in libraryObjectValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
                return 0;
            }
        }

        public bool DeleteLibraryObject(int libraryObjectId)
        {
            _baseDao.DeleteLibraryObject(libraryObjectId);
            return _libraryObjectDao.DeleteLibraryObject(libraryObjectId);
        }

        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            return _libraryObjectDao.GetAllLibraryObjects();
        }

        public LibraryObject GetLibraryObjectById(int libraryObjectId)
        {
            return _libraryObjectDao.GetLibraryObjectById(libraryObjectId);
        }

        public LibraryObject GetLibraryObjectByName(string name)
        {
            return _libraryObjectDao.GetLibraryObjectByName(name);
        }
    }
}
