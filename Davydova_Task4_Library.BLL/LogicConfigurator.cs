﻿using Davydova_Task4_Library.BLL.Implementations;
using Davydova_Task4_Library.BLL.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Davydova_Task4_Library.BLL
{
    public static class LogicConfigurator
    {
        public static IServiceCollection AddAuthorLogic(this IServiceCollection services)
        {
            return services.AddTransient<IAuthorLogic, AuthorLogic>();
        }

        public static IServiceCollection AddBookLogic(this IServiceCollection services)
        {
            return services.AddTransient<IBookLogic, BookLogic>();
        }

        public static IServiceCollection AddLibraryObjectLogic(this IServiceCollection services)
        {
            return services.AddTransient<ILibraryObjectLogic, LibraryObjectLogic>();
        }

        public static IServiceCollection AddNewspaperLogic(this IServiceCollection services)
        {
            return services.AddTransient<INewspaperLogic, NewsPaperLogic>();
        }

        public static IServiceCollection AddPatentLogic(this IServiceCollection services)
        {
            return services.AddTransient<IPatientLogic, PatentLogic>();
        }

        public static IServiceCollection AddPublicationLogic(this IServiceCollection services)
        {
            return services.AddTransient<IPublicationLogic, PublicationLogic>();
        }
    }
}
