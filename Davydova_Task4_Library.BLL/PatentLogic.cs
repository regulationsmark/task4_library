﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class PatentLogic : IPatientLogic
    {
        private IPatentDao _patentDao;
        private IBaseDao _baseDao;
        private IAuthorDao _authorDao;
        private ILogger<PatentLogic> _logger;

        public PatentLogic(IPatentDao patentDao, IBaseDao baseDao, IAuthorDao authorDao, ILogger<PatentLogic> logger)
        {
            _patentDao = patentDao;
            _baseDao = baseDao;
            _authorDao = authorDao;
            _logger = logger;
        }
        public int AddPatent(int id, string name, string countOfPages, string note, string placeOfPublication, List<Author> inventors, string registrationNumber, DateTime applicationDate, DateTime publicationDate, string country)
        {
            PatentValidation patentValidation = new PatentValidation();
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();
            
            if (patentValidation.ExecutePatentValidation(country, registrationNumber, applicationDate, publicationDate, inventors) && libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note))
            {
                foreach (Author inventor in inventors)
                {
                    _authorDao.AddAuthor(inventor);
                }
                Patent patent = new Patent(id, name, int.Parse(countOfPages), note, placeOfPublication, inventors, int.Parse(registrationNumber), applicationDate, publicationDate, country);
                _baseDao.AddLibraryObject(patent);
                return _patentDao.AddPatent(patent);
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in patentValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
                return 0;
            }
        }

        public bool DeletePatent(int idPatent)
        {
            //add checking different authors
            _baseDao.DeleteLibraryObject(idPatent);
            return _patentDao.DeletePatent(idPatent);
        }

        public IEnumerable<Patent> GetAllPatents()
        {
            return _patentDao.GetAllPatents();
        }

        public Patent GetPatentById(int idPatent)
        {
            return _patentDao.GetPatentById(idPatent);
        }

        //add surname of inventor?
        public IEnumerable<Patent> GetPatentsByNameInventor(string name)
        {
            return _baseDao.GetPatentsByInventorId(_authorDao.GetAuthorByName(name).Id).Cast<Patent>();
        }
    }
}
