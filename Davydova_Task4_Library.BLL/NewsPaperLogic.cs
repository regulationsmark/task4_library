﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class NewsPaperLogic : INewspaperLogic
    {
        private INewspaperDao _newspaperDao;
        private IBaseDao _baseDao;
        private ILogger<NewsPaperLogic> _logger;

        public NewsPaperLogic(INewspaperDao newspaperDaoDao, IBaseDao baseDao, ILogger<NewsPaperLogic> logger)
        {
            _newspaperDao = newspaperDaoDao;
            _baseDao = baseDao;
            _logger = logger;
        }
        public int AddNewspaper(int id, string name, string publicationYear, string countOfPages, string note, string placeOfPublication, string publishingHouse, int issueNumber, DateTime issueDate, string iSSN)
        {
            NewspaperValidation newspaperValidation = new NewspaperValidation();
            PublicationValidation publicationValidation = new PublicationValidation();
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();
            
            if (publicationValidation.ExecutePublicationValidation(publishingHouse, publicationYear, placeOfPublication) && libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note))
            {
                if (newspaperValidation.ExecuteNewspaperValidation(iSSN, issueDate, int.Parse(publicationYear)))
                {
                    foreach (NewsPaper newspaperItem in _newspaperDao.GetAllNewspapers())
                    {
                        if (newspaperItem.ISSN == iSSN)
                        {
                            //ConsoleIO.PrintInformation($"ISSN must be unique.");
                            _logger.LogInformation($"ISSN must be unique.");
                            return 0;
                        }
                    }
                    NewsPaper newspaper = new NewsPaper(id, name, int.Parse(publicationYear), int.Parse(countOfPages), note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN);
                    _baseDao.AddLibraryObject(newspaper);
                    return _newspaperDao.AddNewsPaper(newspaper);
                }
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in newspaperValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
            }
            return 0;
        }

        public bool DeleteNewspaper(int newspaperId)
        {
            _baseDao.DeleteLibraryObject(newspaperId);
            return _newspaperDao.DeleteNewsPaper(newspaperId);
        }

        public IEnumerable<NewsPaper> GetAllNewsPapers()
        {
            return _newspaperDao.GetAllNewspapers();
        }

        public NewsPaper GetNewspaperById(int newspaperId)
        {
            return _newspaperDao.GetNewpaperById(newspaperId);
        }
    }
}
