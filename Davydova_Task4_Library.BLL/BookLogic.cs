﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;

namespace Davydova_Task4_Library.BLL.Implementations
{
    public class BookLogic : IBookLogic
    {
        private IBookDao _bookDao;
        private IBaseDao _baseDao;
        private IAuthorDao _authorDao;

        private ILogger<BookLogic> _logger;

        public BookLogic(IBookDao bookDao, IBaseDao baseDao, IAuthorDao authorDao, ILogger<BookLogic> logger)
        {
            _bookDao = bookDao;
            _baseDao = baseDao;
            _authorDao = authorDao;
            _logger = logger;
        }
        public int AddBook(int id, string name, string countOfPages, string note, string placeOfPublication, string publishingHouse, string publishingYear, List<Author> authors, string iSBN)
        {
            BookValidation bookValidation = new BookValidation();
            PublicationValidation publicationValidation = new PublicationValidation();
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();
            try
            {
                //bookValidation.ExecuteBookValidation(iSBN);
                publicationValidation.ExecutePublicationValidation(publishingHouse, publishingYear, placeOfPublication);
                libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note);
                try
                {
                    foreach (Book bookItem in _bookDao.GetAllBooks())
                    {
                        if (bookItem.ISBN == iSBN)
                        {
                            throw new ArgumentException();
                        }
                    }
                    Book book = new Book(id, name, int.Parse(publishingYear), int.Parse(countOfPages), note, placeOfPublication, publishingHouse, authors, iSBN);
                    foreach (Author author in authors)
                    {
                        if (_authorDao.GetAuthorById(author.Id) == null)
                        {
                            _authorDao.AddAuthor(author);
                        }
                    }
                    _baseDao.AddLibraryObject(book);
                    return _bookDao.AddBook(book);
                }
                catch(ArgumentException)
                {
                    //ConsoleIO.PrintInformation($"ISBN must be unique.");
                    _logger.LogInformation($"ISBN must be unique.");
                }
            }
            catch(ValidationException)
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in bookValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
            }

            return 0;
        }

        public bool DeleteBook(int id)
        {
            _baseDao.DeleteLibraryObject(id);//add delete authors
            return _bookDao.DeleteBook(id);
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return _bookDao.GetAllBooks();
        }
        
        public IEnumerable<Book> GetBooksByIdAuthor(int id)
        {
            return _baseDao.GetBooksByAuthorId(_authorDao.GetAuthorById(id).Id);
        }

        public IEnumerable<Book> GetBooksByName(string name)
        {
            return _baseDao.GetLibraryObjectByName(name).Cast<Book>();
        }

        public IEnumerable<Book> GetBooksByNameAuthor(string name)
        {
            return _baseDao.GetBooksByAuthorId(_authorDao.GetAuthorByName(name).Id);
        }

        public void GetReverseSortedBookByPublicationYear()
        {
            _baseDao.ReverseSortByPublicationYear();
        }

        public void GetSortedBookByPublicationYear()
        {
            _baseDao.SortByPublicationYear();
        }

        public IEnumerable<IEnumerable<Book>> GroupByPublicationYear()
        {
            return _baseDao.GroupByPublicationYear().Cast<IEnumerable<Book>>();
        }

        IEnumerable<Book> IBookLogic.GetBooksByCharacterSet(string characterSet)
        {
            return _baseDao.GetBooksStartByChars(characterSet).Cast<Book>();
        }
    }
}
