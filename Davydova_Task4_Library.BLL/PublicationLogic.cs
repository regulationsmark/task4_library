﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class PublicationLogic : IPublicationLogic
    {
        private IPublicationDao _publicationDao;
        private IBaseDao _baseDao;
        private ILogger<PublicationLogic> _logger;

        public PublicationLogic(IPublicationDao publicationDao, IBaseDao baseDao, ILogger<PublicationLogic> logger)
        {
            _publicationDao = publicationDao;
            _baseDao = baseDao;
            _logger = logger;
        }
        public int AddPublication(int id, string name, string publicationYear, string countOfPages, string note, string placeOfPublication, string publishingHouse)
        {
            PublicationValidation publicationValidation = new PublicationValidation();
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();

            if (libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note) && publicationValidation.ExecutePublicationValidation(publishingHouse, publicationYear, placeOfPublication))
            {
                Publication publication = new Publication(id, name, int.Parse(publicationYear), int.Parse(countOfPages), note, placeOfPublication, publishingHouse);
                _baseDao.AddLibraryObject(publication);
                return _publicationDao.AddPublication(publication);
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in publicationValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
                return 0;
            }
        }

        public bool DeletePublication(int publicationId)
        {
            _baseDao.DeleteLibraryObject(publicationId);
            return _publicationDao.DeletePublication(publicationId);
        }

        public IEnumerable<Publication> GetAllPublication()
        {
            return _publicationDao.GetAllPublications();
        }

        public Publication GetPublicationById(int idPublication)
        {
            return _publicationDao.GetPublicationById(idPublication);
        }
    }
}
