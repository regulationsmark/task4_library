﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using System.ComponentModel.DataAnnotations;

namespace Davydova_Task4_Library.BLL.Validations
{
    public class AuthorValidation
    {
        public List<string> _exceptionsList;

        public AuthorValidation()
        {
            _exceptionsList = new List<string>();
        }

        public bool NameValidation(string authorName)
        {
            bool result = true;
            if (string.IsNullOrWhiteSpace(authorName))
            {
                result = false;
                _exceptionsList.Add($"Name cannot be empty.");
            }
            else if (authorName.Length > 50)
            {
                result = false;
                _exceptionsList.Add($"Name cannot be more than 50.");
            }
            else
            {
                var englishAlphabet = @"(^[A-Z][a-z]*$)|(^[A-Z][a-z]*-[A-Z][a-z]*$)";
                var russianAlphabet = @"(^[А-ЯЁ][а-яё]*$)|(^[А-ЯЁ][а-яё]*-[А-ЯЁ][а-яё]*$)";
                if (!Regex.IsMatch(authorName, russianAlphabet) && !Regex.IsMatch(authorName, englishAlphabet))
                {
                    _exceptionsList.Add("The name can contain only Russian and Latin letters and must begin with a capital letter." + Environment.NewLine +
                        "All letters except the first must be small.");
                }
            }
            return result;
        }

        public bool SurnameValidation(string authorSurname)
        {
            bool result = true;
            if (string.IsNullOrWhiteSpace(authorSurname))
            {
                result = false;
                _exceptionsList.Add("Surname cannot be empty.");
            }
            else if (authorSurname.Length > 200)
            {
                result = false;
                _exceptionsList.Add($"Surname cannot be more than 200.");
            }
            else
            {
                var russianAlphabet = @"^([а-яё]* ([А-ЯЁ][а-яё]*)('[А-ЯЁ][а-яё]*)?(-[А-ЯЁ][а-яё]*)?)$|^([а-яё]* ([А-ЯЁ][а-яё]*)(-[А-ЯЁ][а-яё]*('[А-ЯЁ][а-яё]*)?)?)$|^(([А-ЯЁ][а-яё]*)(-[А-ЯЁ][а-яё]*('[А-ЯЁ][а-яё]*)?)?)$|^(([А-ЯЁ][а-яё]*('[А-ЯЁ][а-яё]*)?)(-[А-ЯЁ][а-яё]*)?)$";
                var englishAlphabet = @"^([a-z]* ([A-Z][a-z]*)('[A-Z][a-z]*)?(-[A-Z][a-z]*)?)$|^([a-z]* ([A-Z][a-z]*)(-[A-Z][a-z]*('[A-Z][a-z]*)?)?)$|^(([A-Z][a-z]*)(-[A-Z][a-z]*('[A-Z][a-z]*)?)?)$|^(([A-Z][a-z]*('[A-Z][a-z]*)?)(-[A-Z][a-z]*)?)$";
                if (!Regex.IsMatch(authorSurname, russianAlphabet) && !Regex.IsMatch(authorSurname, englishAlphabet))
                {
                    result = false;
                    _exceptionsList.Add("The surname can contain only Russian and Latin letters and must begin with a capital letter." + Environment.NewLine +
                        "All letters except the first must be small." + Environment.NewLine +
                        "May contain hyphens, apostrophes, and prefixes.");
                }
            }
            return result;
        }

        public bool ExecuteAuthorValidation(string authorName, string authorSurname)
        {
            if (!(NameValidation(authorName) && SurnameValidation(authorSurname)))
            {
                throw new ValidationException();
            }
            return true;
        }
    }
}
