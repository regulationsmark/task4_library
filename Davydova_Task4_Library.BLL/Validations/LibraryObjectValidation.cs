﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Validations
{
    public class LibraryObjectValidation
    {
        public List<string> exceptionsList = new List<string>();

        public bool NameValidation(string name)
        {
            bool result = true;
            if (string.IsNullOrWhiteSpace(name))
            {
                result = false;
                exceptionsList.Add("Name cannot be empty.");
            }
            else if (name.Length > 300)
            {
                result = false;
                exceptionsList.Add("Name length cannot be more than 300.");
            }
            return result;
        }

        public bool CountOfPageValidation(string countOfPages)
        {
            bool result = true;
            int pages;
            if (!string.IsNullOrWhiteSpace(countOfPages))
            {
                bool success = Int32.TryParse(countOfPages, out pages);
                if (success)
                {
                    if (pages < 0)
                    {
                        result = false;
                        exceptionsList.Add("Count of pages cannot be less than  0.");
                    }
                }
                else
                {
                    result = false;
                    exceptionsList.Add("Count of pages must be digits.");
                }
                
            }
            else
            {
                result = false;
                exceptionsList.Add("Count of pages is required field.");
            }
                
            return result;
        }

        public bool NoteValidation(string note)
        {
            bool result = true;
            if (!string.IsNullOrWhiteSpace(note))
            {
                if (note.Length > 2000)
                {
                    result = false;
                    exceptionsList.Add("Note length cannot be more than 2000.");
                }
            }
            return result;
        }

        public bool ExecuteLibraryObjectValidation(string name, string countOfPages, string note)
        {
            return NameValidation(name) && CountOfPageValidation(countOfPages) && NoteValidation(note);
        }
    }
}
