﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.DAL.Interfaces
{
    public interface IAuthorDao
    {
        Author GetAuthorById(int idAuthor);
        Author GetAuthorByName(string nameAuthor);

        int AddAuthor(Author author);

        bool DeleteAuthor(int authorId);

        IEnumerable<Author> GetAllAuthors();

        void UpdateAuthor(Author author);

        IEnumerable<Author> GetAuthorsByBookId(int bookId);

        IEnumerable<Author> GetInventorsByPatentId(int patentId);

    }
}
