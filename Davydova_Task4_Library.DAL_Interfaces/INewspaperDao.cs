﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.Publications;

namespace Davydova_Task4_Library.DAL_Interfaces
{
    public interface INewspaperDao
    {
        NewsPaper GetNewpaperById(int newspaperId);

        int AddNewsPaper(NewsPaper newspaper);

        bool DeleteNewsPaper(int newspaperId);

        IEnumerable<NewsPaper> GetAllNewspapers();
    }
}
