﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class BookDao : SqlDbCommandHelper, IBookDao
    {
        private IConfigurationRoot _configuration;
        private IAuthorDao _authorDao;
        public BookDao(IAuthorDao authorDao)
        {
            _authorDao = authorDao;
            _configuration = DaoConfigurator.BuildConfiguration();
        }

        private static DataTable CreateDtListOfAuthors()
        {
            var dtListOfAuthors = new DataTable("ListOfAuthors");
            dtListOfAuthors.Columns.Add("AuthorId", typeof(int));
            return dtListOfAuthors;
        }
        public int AddBook(Book book)
        {
            var bookId = 0;
            DataTable dtListOfAuthors = CreateDtListOfAuthors();
            foreach (Author author in book.Authors)
            {
                DataRow row = dtListOfAuthors.NewRow();
                row["AuthorId"] = author.Id;
                dtListOfAuthors.Rows.Add(row);
            }
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "AddBook");
                AddParameter(command, CreateParameter("@Name", DbType.String, book.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, book.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, book.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, book.Note));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, book.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, book.PlaceOfPublication));
                AddParameter(command, CreateParameter("@ISBN", DbType.String, book.ISBN));
                //command.AddInputTableParameter("@AuthorsIds", dtListOfAuthors);
                AddParameter(command, CreateParameter("@AuthorsIds", DbType.Object, dtListOfAuthors));
                connection.Open();
                bookId = (int)command.ExecuteScalar();
                Console.WriteLine(bookId);
            }
            return bookId;
        }

        public bool DeleteBook(int bookId)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeleteBookById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, bookId));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            var books = new List<Book>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllBooks");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        List<int> idsAuthors = reader["IdsAuthors"] as List<int>;//?
                        List<Author> authors = new List<Author>();
                        foreach (int idAuthor in idsAuthors)
                        {
                            authors.Add(_authorDao.GetAuthorById(idAuthor));
                        }
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, authors, iSBN);
                        books.Add(book);
                    }
                }
            }
            return books;
        }

        public Book GetBookById(int bookId)
        {
            Book book = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBookById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, bookId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        List<int> idsAuthors = reader["IdsAuthors"] as List<int>;//?
                        List<Author> authors = new List<Author>();
                        foreach (int idAuthor in idsAuthors)
                        {
                            authors.Add(_authorDao.GetAuthorById(idAuthor));
                        }
                        book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, authors, iSBN);
                    }
                }
            }
            return book;
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            var books = new List<Book>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBooksByAuthorId");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        List<int> idsAuthors = reader["IdsAuthors"] as List<int>;//?
                        List<Author> authors = new List<Author>();
                        foreach (int idAuthor in idsAuthors)
                        {
                            authors.Add(_authorDao.GetAuthorById(idAuthor));
                        }
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, authors, iSBN);
                        books.Add(book);
                    }
                }
            }
            return books;
        }

        public void UpdateBook(Book book)
        {
            var result = 0;
            List<int> authorsIds = new List<int>();
            foreach (Author author in book.Authors)
            {
                authorsIds.Add(author.Id);
            }
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "UpdateBook");
                AddParameter(command, CreateParameter("@Name", DbType.String, book.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, book.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, book.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, book.Note));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, book.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, book.PlaceOfPublication));
                AddParameter(command, CreateParameter("@ISBN", DbType.String, book.ISBN));
                AddParameter(command, CreateParameter("@AuthorsIds", DbType.Object, authorsIds));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
        }
    }
}
