﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class BaseDao : SqlDbCommandHelper, IBaseDao
    {
        private IConfigurationRoot _configuration;
        //IBookDao _bookDao;
        //INewspaperDao _newspaperDao;
        //IPatentDao _patentDao;
        IAuthorDao _authorDao;
        public BaseDao(IAuthorDao authorDao)
        {
            _authorDao = authorDao;
            _configuration = DaoConfigurator.BuildConfiguration();
        }

        public int AddLibraryObject(LibraryObject libraryObject)
        {
            var libraryObjectId = 0;

            if (libraryObject is Book)
            {
                //_bookDao.AddBook((Book)libraryObject);
                List<int> authorsIds = new List<int>();
                foreach (Author author in ((Book)libraryObject).Authors)
                {
                    authorsIds.Add(author.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "AddBook");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@ISBN", DbType.String, ((Book)libraryObject).ISBN));
                    AddParameter(command, CreateParameter("@AuthorsIds", DbType.Object, authorsIds));
                    connection.Open();
                    libraryObjectId = (int)command.ExecuteScalar();
                    //Console.WriteLine(libraryObjectId);
                }
            }
            else if (libraryObject is NewsPaper)
            {
                //_newspaperDao.AddNewsPaper((NewsPaper)libraryObject);
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "AddNewspaper");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@IssueNumber", DbType.Int32, ((NewsPaper)libraryObject).IssueNumber));
                    AddParameter(command, CreateParameter("@IssueDate", DbType.DateTime, ((NewsPaper)libraryObject).IssueDate));
                    AddParameter(command, CreateParameter("@ISSN", DbType.String, ((NewsPaper)libraryObject).ISSN));
                    connection.Open();
                    libraryObjectId = (int)command.ExecuteScalar();
                    //Console.WriteLine(libraryObjectId);
                }
            }
            else if (libraryObject is Patent)
            {
                //_patentDao.AddPatent((Patent)libraryObject);
                List<int> inventorsIds = new List<int>();
                foreach (Author inventor in ((Patent)libraryObject).Inventors)
                {
                    inventorsIds.Add(inventor.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "AddPatent");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@Country", DbType.String, ((Patent)libraryObject).Country));
                    AddParameter(command, CreateParameter("@RegistrationNumber", DbType.Int32, ((Patent)libraryObject).RegistrationNumber));
                    AddParameter(command, CreateParameter("@ApplicationDate", DbType.DateTime, ((Patent)libraryObject).ApplicationDate));
                    AddParameter(command, CreateParameter("@PublicationDate", DbType.DateTime, ((Patent)libraryObject).PublicationDate));
                    AddParameter(command, CreateParameter("@InventorsIds", DbType.Object, inventorsIds));
                    connection.Open();
                    libraryObjectId = (int)command.ExecuteScalar();
                    //Console.WriteLine(libraryObjectId);
                }
            }
            return libraryObjectId;
        }

        public bool DeleteLibraryObject(int idLibraryObject)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeleteLibraryObjectById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, idLibraryObject));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public void UpdateLibraryObject(LibraryObject libraryObject)
        {
            var result = 0;
            if (libraryObject is Book)
            {
                //_bookDao.UpdateBook((Book)libraryObject);
                List<int> authorsIds = new List<int>();
                foreach (Author author in ((Book)libraryObject).Authors)
                {
                    authorsIds.Add(author.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "UpdateBook");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@ISBN", DbType.String, ((Book)libraryObject).ISBN));
                    AddParameter(command, CreateParameter("@AuthorsIds", DbType.Object, authorsIds));
                    connection.Open();
                    result = (int)command.ExecuteNonQuery();
                    //Console.WriteLine(result);
                }
            }
            else if (libraryObject is NewsPaper)
            {
                //_newspaperDao.UpdateNewsPaper((NewsPaper)libraryObject);
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "UpdateNewspaper");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@IssueNumber", DbType.Int32, ((NewsPaper)libraryObject).IssueNumber));
                    AddParameter(command, CreateParameter("@IssueDate", DbType.DateTime, ((NewsPaper)libraryObject).IssueDate));
                    AddParameter(command, CreateParameter("@ISSN", DbType.String, ((NewsPaper)libraryObject).ISSN));
                    connection.Open();
                    result = (int)command.ExecuteScalar();
                    //Console.WriteLine(result);
                }
            }
            else if (libraryObject is Patent)
            {
                //_patentDao.UpdatePatent((Patent)libraryObject);
                List<int> inventorsIds = new List<int>();
                foreach (Author inventor in ((Patent)libraryObject).Inventors)
                {
                    inventorsIds.Add(inventor.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "UpdatePatent");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@Country", DbType.String, ((Patent)libraryObject).Country));
                    AddParameter(command, CreateParameter("@RegistrationNumber", DbType.Int32, ((Patent)libraryObject).RegistrationNumber));
                    AddParameter(command, CreateParameter("@ApplicationDate", DbType.DateTime, ((Patent)libraryObject).ApplicationDate));
                    AddParameter(command, CreateParameter("@PublicationDate", DbType.DateTime, ((Patent)libraryObject).PublicationDate));
                    AddParameter(command, CreateParameter("@InventorsIds", DbType.Object, inventorsIds));
                    connection.Open();
                    result = (int)command.ExecuteScalar();
                    //Console.WriteLine(result);
                }
            }
        }

        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            List<LibraryObject> libraryObjects = new List<LibraryObject>();
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllLibraryObjects");
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    switch (reader["TypeLibraryObject"])
                    {
                        case "Book":
                            var id = (int)reader["Id"];
                            var Name = reader["Name"].ToString();
                            var PublicationYear = (int)reader["PublicationYear"];
                            var CountOfPages = (int)reader["CountOfPages"];
                            var Note = reader["Note"].ToString();
                            var PlaceOfPublication = reader["PlaceOfPublication"].ToString();
                            var PublishingHouse = reader["PublishingHouse"].ToString();
                            var Authors = _authorDao.GetAuthorsByBookId((int)reader["Id"]).ToList();
                            var ISBN = reader["ISBN"].ToString();
                            Book book = new Book(id, Name, PublicationYear, CountOfPages, Note, PlaceOfPublication, PublishingHouse, Authors, ISBN);
                            libraryObjects.Add(book);
                            break;
                        case "Patent":
                            var Id = (int)reader["Id"];
                            var patentName = reader["Name"].ToString();
                            var patentPublicationYear = (int)reader["PublicationYear"];
                            var patentCountOfPages = (int)reader["CountOfPages"];
                            var patentPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                            var patentNote = reader["Note"].ToString();
                            var Country = reader["Country"].ToString();
                            var RegistrationNumber = (int)reader["RegistrationNumber"];
                            var ApplicationDate = (DateTime)reader["ApplicationDate"];
                            var PublicationDate = (DateTime)reader["PublicationDate"];
                            var Inventors = _authorDao.GetInventorsByPatentId((int)reader["Id"]).ToList();
                            Patent patent = new Patent(Id, patentName, patentCountOfPages, patentNote, patentPlaceOfPublication, Inventors, RegistrationNumber, ApplicationDate, PublicationDate, Country);
                            libraryObjects.Add(patent);
                            break;
                        case "Newspaper":
                            var newspaperId = (int)reader["Id"];
                            var newspaperName = reader["Name"].ToString();
                            var newspaperPublicationYear = (int)reader["PublicationYear"];
                            var newspaperCountOfPages = (int)reader["CountOfPages"];
                            var newspaperNote = reader["Note"].ToString();
                            var newspaperPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                            var newspaperPublishingHouse = reader["PublishingHouse"].ToString();
                            var IssueNumber = (int)reader["IssueNumber"];
                            var IssueDate = (DateTime)reader["IssueDate"];
                            var ISSN = reader["ISSN"].ToString();
                            NewsPaper newspaper = new NewsPaper(newspaperId, newspaperName, newspaperPublicationYear, newspaperCountOfPages, newspaperNote, newspaperPlaceOfPublication, newspaperPublishingHouse, IssueNumber, IssueDate, ISSN);
                            libraryObjects.Add(newspaper);
                            break;
                    }
                }
            }
            return libraryObjects;
        }

        public IEnumerable<LibraryObject> GetBooksAndPatentsByAuthorId(int authorId)
        {
            var libraryObjects = new List<LibraryObject>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetPatentsAndBooksByAuthorId");
                AddParameter(command, CreateParameter("@Id", DbType.String, authorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var id = (int)reader["Id"];
                                var Name = reader["Name"].ToString();
                                var PublicationYear = (int)reader["PublicationYear"];
                                var CountOfPages = (int)reader["CountOfPages"];
                                var Note = reader["Note"].ToString();
                                var PlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var PublishingHouse = reader["PublishingHouse"].ToString();
                                var Authors = _authorDao.GetAuthorsByBookId((int)reader["Id"]).ToList();
                                var ISBN = reader["ISBN"].ToString();
                                Book bookObject = new Book(id, Name, PublicationYear, CountOfPages, Note, PlaceOfPublication, PublishingHouse, Authors, ISBN);
                                libraryObjects.Add(bookObject);
                                break;
                            case "Patent":
                                var Id = (int)reader["Id"];
                                var patentName = reader["Name"].ToString();
                                var patentPublicationYear = (int)reader["PublicationYear"];
                                var patentCountOfPages = (int)reader["CountOfPages"];
                                var patentPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var patentNote = reader["Note"].ToString();
                                var Country = reader["Country"].ToString();
                                var RegistrationNumber = (int)reader["RegistrationNumber"];
                                var ApplicationDate = (DateTime)reader["ApplicationDate"];
                                var PublicationDate = (DateTime)reader["PublicationDate"];
                                var Inventors = _authorDao.GetInventorsByPatentId((int)reader["Id"]).ToList();
                                Patent patentObject = new Patent(Id, patentName, patentCountOfPages, patentNote, patentPlaceOfPublication, Inventors, RegistrationNumber, ApplicationDate, PublicationDate, Country);
                                libraryObjects.Add(patentObject);
                                break;
                        }
                    }
                }
            }
            return libraryObjects;
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            var books = new List<Book>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBooksByAuthorId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, authorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var bookPublishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        List<int> idsAuthors = reader["IdsAuthors"] as List<int>;//?
                        List<Author> authors = new List<Author>();
                        foreach (int idAuthor in idsAuthors)
                        {
                            authors.Add(_authorDao.GetAuthorById(idAuthor));
                        }
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, bookPublishingHouse, authors, iSBN);
                        books.Add(book);
                    }
                }
            }
            return books;
        }

        public IEnumerable<Book> GetBooksStartByChars(string publishingHouse)
        {
            var books = new List<Book>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBooksByCharacterSetPublishingHouse");
                AddParameter(command, CreateParameter("@CharacterSetPublishingHouse", DbType.String, publishingHouse));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var bookPublishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        List<int> idsAuthors = reader["IdsAuthors"] as List<int>;//?
                        List<Author> authors = new List<Author>();
                        foreach (int idAuthor in idsAuthors)
                        {
                            authors.Add(_authorDao.GetAuthorById(idAuthor));
                        }
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, bookPublishingHouse, authors, iSBN);
                        books.Add(book);
                    }
                }
            }
            return books;
        }

        public IEnumerable<LibraryObject> GetLibraryObjectByName(string nameLibraryObject)
        {
            var libraryObjects = new List<LibraryObject>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetLibraryObjectsBySearchingString");
                AddParameter(command, CreateParameter("@SearchingString", DbType.String, nameLibraryObject));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var id = (int)reader["Id"];
                                var Name = reader["Name"].ToString();
                                var PublicationYear = (int)reader["PublicationYear"];
                                var CountOfPages = (int)reader["CountOfPages"];
                                var Note = reader["Note"].ToString();
                                var PlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var PublishingHouse = reader["PublishingHouse"].ToString();
                                var Authors = _authorDao.GetAuthorsByBookId((int)reader["Id"]).ToList();
                                var ISBN = reader["ISBN"].ToString();
                                Book book = new Book(id, Name, PublicationYear, CountOfPages, Note, PlaceOfPublication, PublishingHouse, Authors, ISBN);
                                libraryObjects.Add(book);
                                break;
                            case "Patent":
                                var Id = (int)reader["Id"];
                                var patentName = reader["Name"].ToString();
                                var patentPublicationYear = (int)reader["PublicationYear"];
                                var patentCountOfPages = (int)reader["CountOfPages"];
                                var patentPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var patentNote = reader["Note"].ToString();
                                var Country = reader["Country"].ToString();
                                var RegistrationNumber = (int)reader["RegistrationNumber"];
                                var ApplicationDate = (DateTime)reader["ApplicationDate"];
                                var PublicationDate = (DateTime)reader["PublicationDate"];
                                var Inventors = _authorDao.GetInventorsByPatentId((int)reader["Id"]).ToList();
                                Patent patent = new Patent(Id, patentName, patentCountOfPages, patentNote, patentPlaceOfPublication, Inventors, RegistrationNumber, ApplicationDate, PublicationDate, Country);
                                libraryObjects.Add(patent);
                                break;
                            case "Newspaper":
                                var newspaperId = (int)reader["Id"];
                                var newspaperName = reader["Name"].ToString();
                                var newspaperPublicationYear = (int)reader["PublicationYear"];
                                var newspaperCountOfPages = (int)reader["CountOfPages"];
                                var newspaperNote = reader["Note"].ToString();
                                var newspaperPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var newspaperPublishingHouse = reader["PublishingHouse"].ToString();
                                var IssueNumber = (int)reader["IssueNumber"];
                                var IssueDate = (DateTime)reader["IssueDate"];
                                var ISSN = reader["ISSN"].ToString();
                                NewsPaper newspaper = new NewsPaper(newspaperId, newspaperName, newspaperPublicationYear, newspaperCountOfPages, newspaperNote, newspaperPlaceOfPublication, newspaperPublishingHouse, IssueNumber, IssueDate, ISSN);
                                libraryObjects.Add(newspaper);
                                break;
                        }

                    }
                }
            }
            return libraryObjects;
        }

        public IEnumerable<Patent> GetPatentsByInventorId(int inventorId)
        {
            var patents = new List<Patent>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetPatentsByInventorId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, inventorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var Id = (int)reader["Id"];
                        var patentName = reader["Name"].ToString();
                        var patentPublicationYear = (int)reader["PublicationYear"];
                        var patentCountOfPages = (int)reader["CountOfPages"];
                        var patentPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                        var patentNote = reader["Note"].ToString();
                        var Country = reader["Country"].ToString();
                        var RegistrationNumber = (int)reader["RegistrationNumber"];
                        var ApplicationDate = (DateTime)reader["ApplicationDate"];
                        var PublicationDate = (DateTime)reader["PublicationDate"];
                        var Inventors = _authorDao.GetInventorsByPatentId((int)reader["Id"]).ToList();
                        Patent patent = new Patent(Id, patentName, patentCountOfPages, patentNote, patentPlaceOfPublication, Inventors, RegistrationNumber, ApplicationDate, PublicationDate, Country);
                        patents.Add(patent);
                    }
                }
            }
            return patents;
        }

        public IEnumerable<LibraryObject> GroupByPublicationYear()
        {
            var libraryObjects = new List<LibraryObject>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetLibraryObjectsGroupedByPublicationYear");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var id = (int)reader["Id"];
                                var Name = reader["Name"].ToString();
                                var PublicationYear = (int)reader["PublicationYear"];
                                var CountOfPages = (int)reader["CountOfPages"];
                                var Note = reader["Note"].ToString();
                                var PlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var PublishingHouse = reader["PublishingHouse"].ToString();
                                var Authors = _authorDao.GetAuthorsByBookId((int)reader["Id"]).ToList();
                                var ISBN = reader["ISBN"].ToString();
                                Book book = new Book(id, Name, PublicationYear, CountOfPages, Note, PlaceOfPublication, PublishingHouse, Authors, ISBN);
                                libraryObjects.Add(book);
                                break;
                            case "Patent":
                                var Id = (int)reader["Id"];
                                var patentName = reader["Name"].ToString();
                                var patentPublicationYear = (int)reader["PublicationYear"];
                                var patentCountOfPages = (int)reader["CountOfPages"];
                                var patentPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var patentNote = reader["Note"].ToString();
                                var Country = reader["Country"].ToString();
                                var RegistrationNumber = (int)reader["RegistrationNumber"];
                                var ApplicationDate = (DateTime)reader["ApplicationDate"];
                                var PublicationDate = (DateTime)reader["PublicationDate"];
                                var Inventors = _authorDao.GetInventorsByPatentId((int)reader["Id"]).ToList();
                                Patent patent = new Patent(Id, patentName, patentCountOfPages, patentNote, patentPlaceOfPublication, Inventors, RegistrationNumber, ApplicationDate, PublicationDate, Country);
                                libraryObjects.Add(patent);
                                break;
                            case "Newspaper":
                                var newspaperId = (int)reader["Id"];
                                var newspaperName = reader["Name"].ToString();
                                var newspaperPublicationYear = (int)reader["PublicationYear"];
                                var newspaperCountOfPages = (int)reader["CountOfPages"];
                                var newspaperNote = reader["Note"].ToString();
                                var newspaperPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var newspaperPublishingHouse = reader["PublishingHouse"].ToString();
                                var IssueNumber = (int)reader["IssueNumber"];
                                var IssueDate = (DateTime)reader["IssueDate"];
                                var ISSN = reader["ISSN"].ToString();
                                NewsPaper newspaper = new NewsPaper(newspaperId, newspaperName, newspaperPublicationYear, newspaperCountOfPages, newspaperNote, newspaperPlaceOfPublication, newspaperPublishingHouse, IssueNumber, IssueDate, ISSN);
                                libraryObjects.Add(newspaper);
                                break;
                        }

                    }
                }
            }
            return libraryObjects;
        }

        public IEnumerable<LibraryObject> ReverseSortByPublicationYear()
        {
            var libraryObjects = new List<LibraryObject>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetReverseSortedBookByPublicationYear");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var id = (int)reader["Id"];
                                var Name = reader["Name"].ToString();
                                var PublicationYear = (int)reader["PublicationYear"];
                                var CountOfPages = (int)reader["CountOfPages"];
                                var Note = reader["Note"].ToString();
                                var PlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var PublishingHouse = reader["PublishingHouse"].ToString();
                                var Authors = _authorDao.GetAuthorsByBookId((int)reader["Id"]).ToList();
                                var ISBN = reader["ISBN"].ToString();
                                Book bookObject = new Book(id, Name, PublicationYear, CountOfPages, Note, PlaceOfPublication, PublishingHouse, Authors, ISBN);
                                libraryObjects.Add(bookObject);
                                break;
                            case "Patent":
                                var Id = (int)reader["Id"];
                                var patentName = reader["Name"].ToString();
                                var patentPublicationYear = (int)reader["PublicationYear"];
                                var patentCountOfPages = (int)reader["CountOfPages"];
                                var patentPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var patentNote = reader["Note"].ToString();
                                var Country = reader["Country"].ToString();
                                var RegistrationNumber = (int)reader["RegistrationNumber"];
                                var ApplicationDate = (DateTime)reader["ApplicationDate"];
                                var PublicationDate = (DateTime)reader["PublicationDate"];
                                var Inventors = _authorDao.GetInventorsByPatentId((int)reader["Id"]).ToList();
                                Patent patentObject = new Patent(Id, patentName, patentCountOfPages, patentNote, patentPlaceOfPublication, Inventors, RegistrationNumber, ApplicationDate, PublicationDate, Country);
                                libraryObjects.Add(patentObject);
                                break;
                        }
                    }
                }
            }
            return libraryObjects;
        }

        public IEnumerable<LibraryObject> SortByPublicationYear()
        {
            var libraryObjects = new List<LibraryObject>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetSortedBookByPublicationYear");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var id = (int)reader["Id"];
                                var Name = reader["Name"].ToString();
                                var PublicationYear = (int)reader["PublicationYear"];
                                var CountOfPages = (int)reader["CountOfPages"];
                                var Note = reader["Note"].ToString();
                                var PlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var PublishingHouse = reader["PublishingHouse"].ToString();
                                var Authors = _authorDao.GetAuthorsByBookId((int)reader["Id"]).ToList();
                                var ISBN = reader["ISBN"].ToString();
                                Book bookObject = new Book(id, Name, PublicationYear, CountOfPages, Note, PlaceOfPublication, PublishingHouse, Authors, ISBN);
                                libraryObjects.Add(bookObject);
                                break;
                            case "Patent":
                                var Id = (int)reader["Id"];
                                var patentName = reader["Name"].ToString();
                                var patentPublicationYear = (int)reader["PublicationYear"];
                                var patentCountOfPages = (int)reader["CountOfPages"];
                                var patentPlaceOfPublication = reader["PlaceOfPublication"].ToString();
                                var patentNote = reader["Note"].ToString();
                                var Country = reader["Country"].ToString();
                                var RegistrationNumber = (int)reader["RegistrationNumber"];
                                var ApplicationDate = (DateTime)reader["ApplicationDate"];
                                var PublicationDate = (DateTime)reader["PublicationDate"];
                                var Inventors = _authorDao.GetInventorsByPatentId((int)reader["Id"]).ToList();
                                Patent patentObject = new Patent(Id, patentName, patentCountOfPages, patentNote, patentPlaceOfPublication, Inventors, RegistrationNumber, ApplicationDate, PublicationDate, Country);
                                libraryObjects.Add(patentObject);
                                break;
                        }
                    }
                }
            }
            return libraryObjects;
        }
    }
}
