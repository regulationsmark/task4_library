﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Davydova_Task4_Library.RealDAL
{
    public class AuthorDao : SqlDbCommandHelper, IAuthorDao
    {
        private IConfigurationRoot _configuration;

        public AuthorDao()
        {
            _configuration = DaoConfigurator.BuildConfiguration();

        }
        public int AddAuthor(Author author)
        {
            int authorId = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "AddAuthor");
                AddParameter(command, CreateParameter("@Name", DbType.String, author.Name));
                AddParameter(command, CreateParameter("@Surname", DbType.String, author.Surname));
                AddParameter(command, CreateParameter("@IsDeleted", DbType.Int64, 0));
                connection.Open();
                authorId = (int)(decimal)command.ExecuteScalar();
            }
            return authorId;
        }

        public bool DeleteAuthor(int authorId)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeleteAuthorById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, authorId));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            var authors = new List<Author>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllAuthors");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var surname = reader["Surname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }
                }
            }
            return authors;
        }

        public Author GetAuthorById(int idAuthor)
        {
            Author author = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAuthorById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, idAuthor));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var surname = reader["Surname"].ToString();
                        author = new Author(id, name, surname);
                    }
                }
            }
            return author;
        }

        public Author GetAuthorByName(string nameAuthor)
        {
            //List<Author> authors = new List<Author>();
            Author author = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAuthorByName");
                AddParameter(command, CreateParameter("@PartName", DbType.String, nameAuthor));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var surname = reader["Surname"].ToString();
                        author = new Author(id, name, surname);
                        //authors.Add(author);
                    }
                }
            }
            return author;
        }

        public void UpdateAuthor(Author author)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "UpdateAuthor");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, author.Id));
                AddParameter(command, CreateParameter("@Name", DbType.String, author.Name));
                AddParameter(command, CreateParameter("@Surname", DbType.String, author.Surname));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
        }

        public IEnumerable<Author> GetAuthorsByBookId(int bookId)
        {
            List<Author> authors = new List<Author>();
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAuthorsByBookId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, bookId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var surname = reader["Surname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }
                }
            }
            return authors;
        }

        public IEnumerable<Author> GetInventorsByPatentId(int patentId)
        {
            List<Author> authors = new List<Author>();
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetInventorsByPatentId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, patentId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var surname = reader["Surname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }
                }
            }
            return authors;
        }

    }
}
