﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class PublicationDao: SqlDbCommandHelper, IPublicationDao
    {
        private IConfigurationRoot _configuration;

        public PublicationDao()
        {
            _configuration = DaoConfigurator.BuildConfiguration();
        }

        public int AddPublication(Publication publication)
        {
            int libraryObjectId = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "AddPublication");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, publication.Id));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, publication.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.Int32, publication.PlaceOfPublication));
                connection.Open();
                libraryObjectId = (int)command.ExecuteScalar();
                //Console.WriteLine(libraryObjectId);
            }
            return libraryObjectId;
        }

        public bool DeletePublication(int publicationId)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeletePublicationById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, publicationId));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Publication> GetAllPublications()
        {
            List<Publication> publications = new List<Publication>();
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllLibraryObjects");
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var id = (int)reader["Id"];
                    var name = reader["Name"].ToString();
                    var publicationYear = (int)reader["PublicationYear"];
                    var countOfPages = (int)reader["CountOfPages"];
                    var note = reader["Note"].ToString();
                    var publishingHouse = reader["PublishingHouse"].ToString();
                    var placeOfPublication = reader["PlaceOfPublication"].ToString();
                    Publication publication = new Publication(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse);
                    publications.Add(publication);
                }
            }
            return publications;
        }

        public Publication GetPublicationById(int publicationId)
        {
            Publication publication = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetPublicationById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, publicationId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        publication = new Publication(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse);
                    }
                }
            }
            return publication;
        }

        public void UpdatePublication(Publication publication)
        {
            var result = 0;

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "UpdatePublication");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, publication.Id));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, publication.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, publication.PlaceOfPublication));
                result = command.ExecuteNonQuery();
            }
        }
    }
}
