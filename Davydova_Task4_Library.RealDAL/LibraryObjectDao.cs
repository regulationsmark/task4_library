﻿using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class LibraryObjectDao: SqlDbCommandHelper, ILibraryObject
    {
        private IConfigurationRoot _configuration;

        public LibraryObjectDao()
        {
            _configuration = DaoConfigurator.BuildConfiguration();
        }

        public int AddLibraryObject(LibraryObject libraryObject)
        {
            int libraryObjectId = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "AddLibraryObject");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, libraryObject.Id));
                AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                connection.Open();
                libraryObjectId = (int)command.ExecuteScalar();
                //Console.WriteLine(libraryObjectId);
            }
            return libraryObjectId;
        }

        public bool DeleteLibraryObject(int libraryObjectId)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeleteOnlyLibraryObjectById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, libraryObjectId));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            List<LibraryObject> libraryObjects = new List<LibraryObject>();
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllLibraryObjects");
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var id = (int)reader["Id"];
                    var Name = reader["Name"].ToString();
                    var PublicationYear = (int)reader["PublicationYear"];
                    var CountOfPages = (int)reader["CountOfPages"];
                    var Note = reader["Note"].ToString();
                    LibraryObject libraryObject = new LibraryObject(id, Name, PublicationYear, CountOfPages, Note);
                    libraryObjects.Add(libraryObject);
                }
            }
            return libraryObjects;
        }

        public LibraryObject GetLibraryObjectById(int idLibraryObject)
        {
            LibraryObject libraryObject = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetLibraryObjectById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, idLibraryObject));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        libraryObject = new LibraryObject(id, name, publicationYear, countOfPages, note);
                    }
                }
            }
            return libraryObject;
        }

        public LibraryObject GetLibraryObjectByName(string name)
        {
            LibraryObject libraryObject = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetLibraryObjectByName");
                AddParameter(command, CreateParameter("@PartName", DbType.String, name));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var libraryObjectName = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        libraryObject = new LibraryObject(id, libraryObjectName, publicationYear, countOfPages, note);
                    }
                }
            }
            return libraryObject;
        }

        public void UpdateLibraryObject(LibraryObject libraryObject)
        {
            var result = 0;
            
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "UpdateLibraryObject");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, libraryObject.Id));
                AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                result = command.ExecuteNonQuery();
            }
        }
    }
}
