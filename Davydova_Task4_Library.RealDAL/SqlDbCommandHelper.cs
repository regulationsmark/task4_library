﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class SqlDbCommandHelper
    {
        public SqlParameter CreateParameter(string parameterName, DbType dbType, object parameterValue)
        {
            return new SqlParameter()
            {
                ParameterName = parameterName,
                DbType = dbType,
                Value = parameterValue
            };
        }

        public void AddParameter(SqlCommand sqlCommand, SqlParameter sqlParameter)
        {
            sqlCommand.Parameters.Add(sqlParameter);
        }

        public SqlCommand CreateCommand(SqlConnection connection, string commandText)
        {
            SqlCommand command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;
            return command;
        }
    }
}
