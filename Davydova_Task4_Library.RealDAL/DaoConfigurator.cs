﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Davydova_Task4_Library.RealDAL
{
    public static class DaoConfigurator
    {
        public static IConfigurationRoot BuildConfiguration()
        {
            string filePath = @"C:\Users\ROKO000000161\OneDrive\Рабочий стол\Repository\roko_task4_library (1)\roko_task4_library\Davydova_Task4_Library.RealDAL\";
            string directoryName = Path.GetDirectoryName(filePath);
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(directoryName)
                .AddJsonFile("appsettings.json")
                .Build();
            return configuration;
        }

        public static IServiceCollection AddAuthorRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IAuthorDao, AuthorDao>();
        }

        public static IServiceCollection AddBookRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IBookDao, BookDao>();
        }

        public static IServiceCollection AddBaseRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IBaseDao, BaseDao>();
        }

        public static IServiceCollection AddLibraryObjectRealDao(this IServiceCollection services)
        {
            return services.AddTransient<ILibraryObject, LibraryObjectDao>();
        }
        public static IServiceCollection AddPublicationRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IPublicationDao, PublicationDao>();
        }
    }
}
